<?php

namespace Kudze\KafkaConsumerProducer\Services;

use RdKafka\Conf as KConf;
use RdKafka\Exception as KException;
use RdKafka\Producer as KProducer;

class KafkaProducer
{
    protected ?KProducer $producer = null;
    protected array $topics = [];

    public function init(): void
    {
        $this->producer = new KProducer($this->configure());
    }

    /**
     * @throws KException
     */
    public function produce(
        string  $payload,
        string  $topic,
        ?string $key = null,
        int     $partition = RD_KAFKA_PARTITION_UA,
        int     $msgflags = 0
    ): void
    {
        if (!array_key_exists($topic, $this->topics))
            $this->topics[$topic] = $this->producer->newTopic($topic);

        try {
            $this->topics[$topic]->produce($partition, $msgflags, $payload, $key);
        } catch (KException $exception) {
            switch ($exception->getCode()) {
                //if queue is full then we flush buffer and produce again.
                case RD_KAFKA_RESP_ERR__QUEUE_FULL:
                    $this->flush();
                    $this->topics[$topic]->produce($partition, $msgflags, $payload, $key);
                    break;
                default:
                    throw $exception;
            }
        }
    }

    public function flush(int $timeout = -1): void
    {
        $this->producer->flush($timeout);
    }

    protected function configure(): KConf
    {
        $conf = new KConf();
        $conf->set('metadata.broker.list', $_ENV['KAFKA_BROKERS']);

        $securityProtocol = array_key_exists('KAFKA_SECURITY_PROTOCOL', $_ENV) ? $_ENV['KAFKA_SECURITY_PROTOCOL'] : 'PLAINTEXT';
        $conf->set('security.protocol', $securityProtocol);

        $caAuthority = array_key_exists('KAFKA_SECURITY_CA_AUTHORITY', $_ENV) ? $_ENV['KAFKA_SECURITY_CA_AUTHORITY'] : null;
        if ($caAuthority)
            $conf->set('ssl.ca.location', $caAuthority);

        return $conf;
    }
}
