<?php

namespace Kudze\KafkaConsumerProducer\Services;

use RdKafka\Conf as KConf;
use RdKafka\Exception as KException;
use RdKafka\KafkaConsumer as KConsumer;
use RdKafka\Message as KMessage;

class KafkaConsumer
{
    protected ?KConsumer $consumer = null;

    /**
     * @throws KException
     */
    public function subscribe(array $topics): void
    {
        $this->consumer = new KConsumer($this->configure());
        $this->consumer->subscribe($topics);
    }

    /**
     * @throws KException
     */
    public function consume(?int $timeoutMs = null): KMessage
    {
        return $this->consumer->consume($timeoutMs ?? $this->getDefaultTimeout());
    }

    protected function configure(): KConf
    {
        $conf = new KConf();
        $conf->set('group.id', $_ENV['KAFKA_GROUP_ID']);
        $conf->set('metadata.broker.list', $_ENV['KAFKA_BROKERS']);
        $conf->set('auto.offset.reset', 'earliest');
        $conf->set('enable.partition.eof', 'false');

        $securityProtocol = array_key_exists('KAFKA_SECURITY_PROTOCOL', $_ENV) ? $_ENV['KAFKA_SECURITY_PROTOCOL'] : 'PLAINTEXT';
        $conf->set('security.protocol', $securityProtocol);

        $caAuthority = array_key_exists('KAFKA_SECURITY_CA_AUTHORITY', $_ENV) ? $_ENV['KAFKA_SECURITY_CA_AUTHORITY'] : null;
        if ($caAuthority)
            $conf->set('ssl.ca.location', $caAuthority);

        return $conf;
    }

    protected function getDefaultTimeout(): int
    {
        return array_key_exists('KAFKA_TIMEOUT', $_ENV) ? (int)$_ENV['KAFKA_TIMEOUT'] : -1;
    }

    /**
     * @return KConsumer
     */
    public function getConsumer(): KConsumer
    {
        return $this->consumer;
    }
}
